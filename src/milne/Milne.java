package milne;

import java.util.ArrayList;

public class Milne {


    private static double function(double x, double y){
        return 1.6*x + 0.5*y*y;
    }

    public static int milne(double x0, double y0, double x, double eps){
        ArrayList<Double[]> result = new ArrayList<>();
        double step = (x - x0), currentX;
        double pred, corr;
        int size = 0;
        while(true) {
            currentX = x0;
            step /= 10;
            int preres = rungeKutta(result, step, currentX, y0);
            if (preres != 0) return preres;
            currentX += 3 * step;
            while (currentX <= x) {
                currentX += step;
                size = result.size();
                pred = result.get(size - 4)[1] + (4 * step / 3) * (2 * function(result.get(size - 3)[0], result.get(size - 3)[1]) -
                        function(result.get(size - 2)[0], result.get(size - 2)[1]) + 2 * function(result.get(size - 1)[0], result.get(size - 1)[1]));
                corr = result.get(size - 2)[1] + (step / 3) * (function(result.get(size - 2)[0], result.get(size - 2)[1]) +
                        4 * function(result.get(size - 1)[0], result.get(size - 1)[1]) + function(currentX, pred));
                if (Math.abs(pred - corr)/29 > eps) break;
                if (corr == Double.POSITIVE_INFINITY){
                    return 2;
                }
                result.add(new Double[]{currentX, corr});
            }
            if (currentX >= x) break;
            else {
                if (size > 100000){
                    return 1;
                }
            }
        }
        Data.setPoints(result);
        return 0;
    }

    private static int rungeKutta(ArrayList<Double[]> result, double step, double x0, double y0){
        result.clear();
        result.add(new Double[]{x0, y0});
        double x = x0, y = y0, k1, k2, k3, k4;
        for (int i = 0; i < 3; i++){

            k1 = step*function(x, y);
            k2 = step*function(x + step/2, y + k1/2);
            k3 = step*function(x + step/2, y + k2/2);
            k4 = step*function(x + step, y + k3);
            y += (k1 + 2*k2 + 2*k3 + k4)/6;
            if (Math.abs(y) == Double.POSITIVE_INFINITY){
                return 2;
            }
            x += step;
            result.add(new Double[]{x, y});
        }
        return 0;

    }
}
