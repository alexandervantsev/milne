package milne;

import java.util.ArrayList;

public class Data {
    private static ArrayList<Double[]> points = new ArrayList<>();

    public static ArrayList<Double[]> getPoints() {
        return points;
    }

    public static void setPoints(ArrayList<Double[]> points) {
        Data.points = points;
    }
}
