package milne;


import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

class MainWindow extends JFrame{
    private static JTextPane message = new JTextPane();
    MainWindow(){

        super("Milne");
        Dimension dim = new Dimension(680,640),
                dimComboBox = new Dimension(60,30),
                dimMessage = new Dimension(500,20);
        this.setMinimumSize(dim);
        this.setResizable(false);
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        MyCanvas myGraphics = new MyCanvas();

        JTextField x0TextField = new JTextField(),
                y0TextField = new JTextField(),
                xTextField = new JTextField(),
                eTextField = new JTextField();
        JLabel x0Label = new JLabel("x0: "),
                y0Label = new JLabel("y0: "),
                xLabel = new JLabel("X: "),
                eLabel = new JLabel(String.valueOf('\u03B5'));


        JButton okButton = new JButton("OK");

        message.setEditable(false);
        message.setMaximumSize(dimMessage);



        x0TextField.setMaximumSize(dimComboBox);
        y0TextField.setMaximumSize(dimComboBox);
        xTextField.setMaximumSize(dimComboBox);
        eTextField.setMaximumSize(dimComboBox);



        okButton.addActionListener(e -> {
            double x0, y0, x, eps;
            try {
                x0 = Double.parseDouble(x0TextField.getText());
                y0 = Double.parseDouble(y0TextField.getText());
                x = Double.parseDouble(xTextField.getText());
                eps = Double.parseDouble(eTextField.getText());
            } catch (NumberFormatException nfe){
                message.setText("All arguments must be decimal numbers.");
                return;
            } catch (NullPointerException npe){
                message.setText("All arguments must be set.");
                return;
            }
            if (x == x0){
                message.setText("X cannot be equal to x0");
                return;
            }
            int answer = Milne.milne(x0,y0,x,eps);
            if (answer == 0) {
                myGraphics.repaint();
            }
            printError(answer);

        });


        layout.setHorizontalGroup(layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addComponent(x0Label)
                                        .addComponent(x0TextField))
                                .addGroup(layout.createSequentialGroup()
                                        .addComponent(y0Label)
                                        .addComponent(y0TextField))
                                .addGroup(layout.createSequentialGroup()
                                        .addComponent(xLabel)
                                        .addComponent(xTextField))
                                .addGroup(layout.createSequentialGroup()
                                        .addComponent(eLabel)
                                        .addComponent(eTextField))
                                .addComponent(okButton))
                        .addComponent(myGraphics))
                .addComponent(message)
        );

        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup()
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup()
                                        .addComponent(x0Label)
                                        .addComponent(x0TextField))
                                .addGroup(layout.createParallelGroup()
                                        .addComponent(y0Label)
                                        .addComponent(y0TextField))
                                .addGroup(layout.createParallelGroup()
                                        .addComponent(xLabel)
                                        .addComponent(xTextField))
                                .addGroup(layout.createParallelGroup()
                                        .addComponent(eLabel)
                                        .addComponent(eTextField))
                                .addComponent(okButton))
                        .addComponent(myGraphics))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(message)));

        pack();
        setVisible(true);
    }

    public static void printError(int err){
        String msg = "Succesful";
        switch (err){
            case 1: msg = "Cannot calculate the answer with the required accuracy. Step must be too little."; break;
            case 2: msg = "Cannot calculate the answer. Calculated y is infinite or is not a real number."; break;
        }
        message.setText(msg);
    }


}
