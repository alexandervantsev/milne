package milne;

import java.util.ArrayList;
import java.util.function.Function;

public class Lagrange {
    public static double lagrange(ArrayList<Double> ords, ArrayList<Double> args, double x){
        double _result = 1, result = 0;
        for (int i = 0; i < args.size(); i++){
            _result = 1;
            for (int j = 0; j < args.size(); j++) {
                if (i != j)
                    _result *= (x - args.get(j)) / (args.get(i)-args.get(j));
            }
            result += ords.get(i)*_result;
        }
        return result;
    }
}
