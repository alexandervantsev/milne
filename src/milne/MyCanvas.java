package milne;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import javax.swing.*;
public class MyCanvas extends JComponent {
    private int width = 500, height = 500, offset = 20;
    private int xCoordOfYAxis, yCoordOfXAxis;
    private double scaleX, scaleY;
    MyCanvas() {
        setLayout(null);
        setSize(width, height);
        setVisible(true);
    }

    public void paint(Graphics my_picture) {
        paintBackground(my_picture);
        if (Data.getPoints().size()>=1) {
            paintAxes(my_picture);
            paintLagrange(my_picture);
        }
    }

    private void paintBackground(Graphics my_picture){
        my_picture.setColor(Color.WHITE);
        my_picture.fillRect(0, 0, 500, 500);
    }

    private void paintAxes(Graphics my_picture){
        int argsCount = Data.getPoints().size();
        double minX = Data.getPoints().get(0)[0],
                maxX = Data.getPoints().get(argsCount - 1)[0],
                minY = Data.getPoints().get(0)[1],
                maxY = Data.getPoints().get(0)[1];
        for (int i = 0; i < argsCount; i++){
            if (Data.getPoints().get(i)[1] > maxY) maxY = Data.getPoints().get(i)[1];
            if (Data.getPoints().get(i)[1] < minY) minY = Data.getPoints().get(i)[1];
        }


        int firstCutX, firstCutY;
        my_picture.setColor(Color.BLACK);
        if (minX >= 0){
            scaleX = (width - offset) / (maxX);
            firstCutX = 30;
            xCoordOfYAxis = offset / 2;
        } else if (maxX <= 0){
            scaleX = (width - offset) / Math.abs(minX);
            firstCutX = - 30;
            xCoordOfYAxis = width - offset / 2;
        } else {
            scaleX = (width - offset) / (maxX - minX);
            firstCutX = (maxX*scaleX > 30)? 30: -30;
            xCoordOfYAxis = width - offset - (int) (maxX * scaleX);
        }
        if (minY >= 0){
            scaleY = (height - offset) / (maxY);
            firstCutY = - 30;
            yCoordOfXAxis = height - offset / 2;
        } else if (maxY <= 0){
            scaleY = (height - offset) / Math.abs(minY);
            firstCutY = 30;
            yCoordOfXAxis = offset / 2;
        } else {
            scaleY = (height - offset) / (maxY - minY);
            firstCutY = (maxY*scaleY > 30)? - 30: 30;
            yCoordOfXAxis = offset + (int) (maxY * scaleY);
        }
        paintYAxis(my_picture, firstCutY, scaleY);
        paintXAxis(my_picture, firstCutX, scaleX);
    }


    private void paintLagrange(Graphics my_picture){
        int argsCount = Data.getPoints().size();
        double start = Data.getPoints().get(0)[0];
        double end = Data.getPoints().get(argsCount - 1)[0];
        my_picture.setColor(Color.BLUE);
        double absLength = end - start;
        ArrayList<Double> x = new ArrayList<>(), y = new ArrayList<>(), yForCanvas = new ArrayList<>();
        for (int i = 0; i < argsCount; i++){
            x.add(Data.getPoints().get(i)[0]);
            y.add(Data.getPoints().get(i)[1]);
        }
        double step = absLength * 1.1 / 100;
        double currentX = start - step;
        for (int i = 0; i < 100; i++){
            yForCanvas.add(-Lagrange.lagrange(y, x, currentX)*scaleY  + yCoordOfXAxis );
            currentX += step;
        }
        currentX = (start-step)*scaleX + xCoordOfYAxis;
        step = absLength * 1.1 / 100*scaleX;
        for (int i = 1; i < 100; i++){
            my_picture.drawLine((int)currentX,(int)(0 + yForCanvas.get(i-1)),(int)(currentX+step),(int)(0 + yForCanvas.get(i)));
            currentX += step;
        }
        for (int i = 0; i < Data.getPoints().size(); i++){
            double _x = Data.getPoints().get(i)[0];
            double _y = Data.getPoints().get(i)[1];
            int posX =  (int) (_x*scaleX) + xCoordOfYAxis ;
            int posY = (int) (-_y*scaleY)  + yCoordOfXAxis;
            my_picture.fillOval(posX - 2, posY-2, 4, 4);
        }
    }


    private void paintYAxis(Graphics my_picture, int firstCut, double scale){
        my_picture.drawLine(xCoordOfYAxis, offset/2, xCoordOfYAxis, height - offset/2);
        my_picture.drawLine(xCoordOfYAxis, offset/2, xCoordOfYAxis - 6, offset/2 + 6);
        my_picture.drawLine(xCoordOfYAxis, offset/2, xCoordOfYAxis + 6, offset/2 + 6);
        my_picture.drawString("y",xCoordOfYAxis + 8,offset/2);
        my_picture.drawLine(xCoordOfYAxis - 4, yCoordOfXAxis + firstCut, xCoordOfYAxis +4, yCoordOfXAxis + firstCut);
        String scaleY = String.valueOf(-firstCut/scale);
        scaleY = scaleY.substring(0, scaleY.indexOf('.')+4);
        my_picture.drawString(scaleY, xCoordOfYAxis + 10,yCoordOfXAxis + firstCut);
    }
    private void paintXAxis(Graphics my_picture, int firstCut, double scale){
        my_picture.drawLine(offset/2, yCoordOfXAxis, width - offset/2, yCoordOfXAxis);
        my_picture.drawLine(width - offset/2, yCoordOfXAxis, width - offset/2 - 6, yCoordOfXAxis - 6);
        my_picture.drawLine(width - offset/2, yCoordOfXAxis, width - offset/2 - 6, yCoordOfXAxis + 6);
        my_picture.drawString("x",width - offset/2 - 6,yCoordOfXAxis - 10);
        my_picture.drawLine(firstCut + xCoordOfYAxis, yCoordOfXAxis - 4, firstCut + xCoordOfYAxis, yCoordOfXAxis +4);
        String scaleX = String.valueOf(firstCut/scale);
        scaleX = scaleX.substring(0, scaleX.indexOf('.')+3);
        my_picture.drawString(scaleX, firstCut + xCoordOfYAxis,yCoordOfXAxis + 15);
    }
}